"""Entry point for canter."""

from pathlib import Path
import sys
path_root = Path(__file__).parents[1]
sys.path.append(str(path_root))


from canter.main import main  # pragma: no cover

if __name__ == "__main__":  # pragma: no cover
    main()
