"""
canter base module.

This is the principal module of the canter project.
here you put your main classes and objects.

Be creative! do whatever you want!

If you want to replace this with a Flask application run:

    $ make init

and then choose `flask` as template.
"""

# example constant variable
NAME = "canter"

TURBO_SWITCHING_INTERVAL = 500
SIGNAL_BUFFER_INTERVAL = 500

RELAY_GPIO_PIN_1 = 26
RELAY_GPIO_PIN_2 = 20
RELAY_GPIO_PIN_3 = 21

BUTTON_2IN_GPIO_PIN = 17
BUTTON_4IN_GPIO_PIN = 4
BUTTON_TURBO_GPIO_PIN = 18

STATUS_READY_GPI_PIN = 16
STATUS_TURBO_GPIO_PIN = 12
STATUS_2IN_GPIO_PIN = 19
STATUS_4IN_GPIO_PIN = 6
