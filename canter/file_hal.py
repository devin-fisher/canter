import json
import os
import time

from canter.hal import BufferedSignalHal
from canter.state import State, Signal

from pathlib import Path
path_root = Path(__file__).parents[1]

DEFAULT_PATH = path_root.joinpath("tests", "assets", "hal_signal.json")


def write_default_file():
    if not os.path.isfile(DEFAULT_PATH):
        with open(DEFAULT_PATH, 'w') as f:
            f.write("""{
"pipe_2_inch": false,
"pipe_4_inch": false,
"turbo": true
}""")


def read_file():
    with open(DEFAULT_PATH, 'r') as f:
        data = json.load(f)
        rtn = Signal(
            data.get("pipe_2_inch", False),
            data.get("pipe_4_inch", False),
            data.get("turbo", False),
        )

        return rtn


class FileHal(BufferedSignalHal):

    def setup(self) -> any:
        write_default_file()

    def cleanup(self):
        pass

    def do_change_state(self, current_state, next_state) -> State:
        return next_state

    def current_signals(self) -> Signal:
        time.sleep(.5)
        return read_file()
