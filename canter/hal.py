import signal, sys

from abc import ABC, abstractmethod

from canter.base import SIGNAL_BUFFER_INTERVAL
from canter.state import State, Signal, current_ms, more_than


class Hal(ABC):
    killed = False

    @abstractmethod
    def signals(self) -> Signal:
        pass

    @abstractmethod
    def change_state(self, current_state, next_state) -> State:
        pass
    @abstractmethod
    def setup(self):
        pass

    @abstractmethod
    def cleanup(self):
        pass

    def _handler(self, signum, frame):
        self.killed = True

    def __enter__(self):
        signal.signal(signal.SIGINT, self._handler)
        signal.signal(signal.SIGTERM, self._handler)
        self.setup()
        return self

    def __exit__(self, *args):
        self.cleanup()
        if self.killed:
            sys.exit(0)


class BufferedSignalHal(Hal, ABC):
    buffer: Signal = None
    buffer_timestamp: int = 0

    def signals(self) -> Signal:
        current = self.current_signals()
        rtn = None

        if current != self.buffer:
            self.buffer = current

            # print new signal values
            print(
                f'P2: {current.pipe_2_inch} -- P4: {current.pipe_4_inch} -- T: {current.turbo}'
            )

            self.buffer_timestamp = current_ms()
            rtn = None

        if more_than(self.buffer_timestamp, SIGNAL_BUFFER_INTERVAL):
            rtn = current

        return rtn

    def change_state(self, current_state, next_state) -> State:
        if current_state != next_state:
            print(
                f'M1: {next_state.motor1.energized} -- M2: {next_state.motor2.energized} -- M3: {next_state.motor3.energized} -- T: {next_state.turbo}'
            )

        return self.do_change_state(current_state, next_state)

    @abstractmethod
    def current_signals(self) -> Signal:
        pass

    @abstractmethod
    def do_change_state(self, current_state, next_state) -> State:
        pass


