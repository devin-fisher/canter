from canter import signals, starting, duty, hal
from canter import rasbpi_hal
from canter.state import State, off



def main():  # pragma: no cover
    """
    Main command loop for the motor controller
    """
    state = off()
    # hardware abstraction layer
    with rasbpi_hal.RaspberryPi() as h:
        while True:
            next_state = pipeline(state, h)

            state = h.change_state(state, next_state)


    # hal_provider = file_hal.FileHal()






def pipeline(current_state, hal_provider) -> State:
    next_state = None

    next_state = signals.next_computed_state(current_state, next_state, hal_provider)

    if next_state is None:
        # TODO Error State, signal should never produce a None next state
        pass

    next_state = starting.next_computed_state(current_state, next_state, hal_provider)

    if next_state is None:
        # TODO Error State, starting should never produce a None next state
        pass

    next_state = duty.next_computed_state(current_state, next_state, hal)

    if next_state is None:
        # TODO Error State, duty should never produce a None next state
        pass

    return next_state
