import time

import RPi.GPIO as GPIO

from canter.base import *
from canter.hal import BufferedSignalHal
from canter.state import State, Signal

RELAY_ON = GPIO.LOW
RELAY_OFF = GPIO.HIGH

LED_ON = GPIO.HIGH
LED_OFF = GPIO.LOW


class RaspberryPi(BufferedSignalHal):

    def setup(self) -> any:
        GPIO.setwarnings(False)
        GPIO.setmode(GPIO.BCM)

        GPIO.setup(RELAY_GPIO_PIN_1, GPIO.OUT)
        GPIO.setup(RELAY_GPIO_PIN_2, GPIO.OUT)
        GPIO.setup(RELAY_GPIO_PIN_3, GPIO.OUT)

        GPIO.setup(STATUS_READY_GPI_PIN, GPIO.OUT)
        GPIO.setup(STATUS_TURBO_GPIO_PIN, GPIO.OUT)
        GPIO.setup(STATUS_2IN_GPIO_PIN, GPIO.OUT)
        GPIO.setup(STATUS_4IN_GPIO_PIN, GPIO.OUT)

        GPIO.setup(BUTTON_2IN_GPIO_PIN, GPIO.IN)
        GPIO.setup(BUTTON_4IN_GPIO_PIN, GPIO.IN)
        GPIO.setup(BUTTON_TURBO_GPIO_PIN, GPIO.IN)

        GPIO.output(STATUS_READY_GPI_PIN, RELAY_OFF)
        GPIO.output(STATUS_TURBO_GPIO_PIN, RELAY_OFF)
        GPIO.output(STATUS_2IN_GPIO_PIN, RELAY_OFF)
        GPIO.output(STATUS_4IN_GPIO_PIN, RELAY_OFF)

        GPIO.output(RELAY_GPIO_PIN_1, RELAY_OFF)
        GPIO.output(RELAY_GPIO_PIN_2, RELAY_OFF)
        GPIO.output(RELAY_GPIO_PIN_3, RELAY_OFF)

        GPIO.output(STATUS_READY_GPI_PIN, LED_OFF)
        GPIO.output(STATUS_TURBO_GPIO_PIN, LED_OFF)
        GPIO.output(STATUS_2IN_GPIO_PIN, LED_OFF)
        GPIO.output(STATUS_4IN_GPIO_PIN, LED_OFF)

        time.sleep(.1)

        GPIO.output(STATUS_READY_GPI_PIN, LED_ON)

    def cleanup(self):
        GPIO.cleanup()

    def current_signals(self) -> Signal:
        pipe_2_inch = False
        pipe_4_inch = False
        turbo = False

        if GPIO.input(BUTTON_2IN_GPIO_PIN):
            GPIO.output(STATUS_2IN_GPIO_PIN, LED_ON)
            pipe_2_inch = True
        else:
            GPIO.output(STATUS_2IN_GPIO_PIN, LED_OFF)

        if GPIO.input(BUTTON_4IN_GPIO_PIN):
            GPIO.output(STATUS_4IN_GPIO_PIN, LED_ON)
            pipe_4_inch = True
        else:
            GPIO.output(STATUS_4IN_GPIO_PIN, LED_OFF)

        if GPIO.input(BUTTON_TURBO_GPIO_PIN):
            turbo = True

        return Signal(pipe_2_inch, pipe_4_inch, turbo)

    def do_change_state(self, current_state, next_state) -> State:
        if next_state.motor1.energized:
            GPIO.output(RELAY_GPIO_PIN_1, RELAY_ON)
        else:
            GPIO.output(RELAY_GPIO_PIN_1, RELAY_OFF)

        if next_state.motor2.energized:
            GPIO.output(RELAY_GPIO_PIN_2, RELAY_ON)
        else:
            GPIO.output(RELAY_GPIO_PIN_2, RELAY_OFF)

        if next_state.motor3.energized:
            GPIO.output(RELAY_GPIO_PIN_3, RELAY_ON)
        else:
            GPIO.output(RELAY_GPIO_PIN_3, RELAY_OFF)

        if next_state.turbo:
            GPIO.output(STATUS_TURBO_GPIO_PIN, LED_ON)
        else:
            GPIO.output(STATUS_TURBO_GPIO_PIN, LED_OFF)

        return next_state
