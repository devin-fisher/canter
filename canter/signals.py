"""
Compute the next state based on the current signals
"""
from canter import state
from canter.hal import Hal


def next_computed_state(current_state, _, hal: Hal):
    signals = hal.signals()
    if signals is not None:
        if signals.turbo:
            return current_state.switch_turbo()
        else:
            motors_needed = calculate_needed_motors(current_state, signals)

            return state.find_motors(current_state, motors_needed)
    else:
        return current_state


def calculate_needed_motors(current_state, signals):
    motors_needed = 0
    if signals.pipe_2_inch:
        motors_needed = motors_needed + 1

    if signals.pipe_4_inch:
        motors_needed = motors_needed + 2

    if current_state.turbo and motors_needed > 0:
        motors_needed = motors_needed + 1

    if motors_needed > 3:
        motors_needed = 3

    return motors_needed
