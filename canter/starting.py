"""
modify the computed next state based on if motors are currently starting up
"""
from canter.hal import Hal
from canter.state import State


def next_computed_state(current_state: State, next_state: State, hal: Hal):
    return next_state
