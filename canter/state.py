from dataclasses import dataclass

from canter.base import TURBO_SWITCHING_INTERVAL
from canter.time import current_ms, more_than


@dataclass(frozen=True)
class Signal:
    def __eq__(self, __value):
        if type(__value) is not Signal:
            return False
        return (
                (self.pipe_2_inch == __value.pipe_2_inch)
                and (self.pipe_4_inch == __value.pipe_4_inch)
                and (self.turbo == __value.turbo)
        )

    pipe_2_inch: bool
    pipe_4_inch: bool
    turbo: bool


@dataclass(frozen=True)
class MotorState:
    energized: bool
    start_timestamp: any
    stop_timestamp: any

    def ms_running(self) -> int:
        return current_ms() - self.start_timestamp


@dataclass(frozen=True)
class State:
    motor1: MotorState
    motor2: MotorState
    motor3: MotorState
    turbo: bool = False
    turbo_switch_timestamp: int = 0

    def switch_turbo(self):
        if more_than(self.turbo_switch_timestamp, TURBO_SWITCHING_INTERVAL):
            return State(
                self.motor1,
                self.motor2,
                self.motor3,
                not self.turbo,
                current_ms(),
            )
        else:
            return self

    def de_energize_motor1(self):
        return State(
            MotorState(False, self.motor1.start_timestamp, self.motor1.stop_timestamp),
            self.motor2,
            self.motor3,
            self.turbo,
            self.turbo_switch_timestamp
        )

    def energize_motor1(self):
        return State(
            MotorState(True, self.motor1.start_timestamp, self.motor1.stop_timestamp),
            self.motor2,
            self.motor3,
            self.turbo,
            self.turbo_switch_timestamp
        )

    def de_energize_motor2(self):
        return State(
            self.motor1,
            MotorState(False, self.motor2.start_timestamp, self.motor2.stop_timestamp),
            self.motor3,
            self.turbo,
            self.turbo_switch_timestamp
        )

    def energize_motor2(self):
        return State(
            self.motor1,
            MotorState(True, self.motor2.start_timestamp, self.motor2.stop_timestamp),
            self.motor3,
            self.turbo,
            self.turbo_switch_timestamp
        )

    def de_energize_motor3(self):
        return State(
            self.motor1,
            self.motor2,
            MotorState(False, self.motor3.start_timestamp, self.motor3.stop_timestamp),
            self.turbo,
            self.turbo_switch_timestamp
        )

    def energize_motor3(self):
        return State(
            self.motor1,
            self.motor2,
            MotorState(True, self.motor3.start_timestamp, self.motor3.stop_timestamp),
            self.turbo,
            self.turbo_switch_timestamp
        )

    def count_energized_motors(self):
        rtn = 0
        if self.motor1.energized:
            rtn += 1
        if self.motor2.energized:
            rtn += 1
        if self.motor3.energized:
            rtn += 1
        return rtn


def find_motors(current_state: State, motors_needed: int):
    if current_state is None:
        return off()

    if current_state.count_energized_motors() == motors_needed:
        return current_state
    elif current_state.count_energized_motors() > motors_needed:
        return find_motors(remove_motor(current_state), motors_needed)
    elif current_state.count_energized_motors() < motors_needed:
        return find_motors(add_motor(current_state), motors_needed)

    return off()


def remove_motor(current_state):
    if current_state.motor1.energized:
        return current_state.de_energize_motor1()
    if current_state.motor2.energized:
        return current_state.de_energize_motor2()
    if current_state.motor3.energized:
        return current_state.de_energize_motor3()

    return current_state


def add_motor(current_state):
    if not current_state.motor1.energized:
        return current_state.energize_motor1()
    if not current_state.motor2.energized:
        return current_state.energize_motor2()
    if not current_state.motor3.energized:
        return current_state.energize_motor3()
    return current_state


def off():
    return State(
        MotorState(False, None, None),
        MotorState(False, None, None),
        MotorState(False, None, None)
    )
