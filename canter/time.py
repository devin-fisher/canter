import time


def current_ms() -> int:
    return int(time.time() * 1000)


def more_than(timestamp, interval) -> bool:
    c = current_ms()
    return c - timestamp >= interval
