import random

from canter.state import State, MotorState


motor_list = [1, 2, 3]


def generate_current_state(energized_count=1, placement_randomized=True, turbo=False):
    if placement_randomized:
        selected = random.sample(motor_list, k=energized_count)
    else:
        selected = list(range(energized_count+1))

    m1 = MotorState(True, None, None) if 1 in selected else MotorState(False, None, None)
    m2 = MotorState(True, None, None) if 2 in selected else MotorState(False, None, None)
    m3 = MotorState(True, None, None) if 3 in selected else MotorState(False, None, None)

    rtn = State(m1, m2, m3, turbo)
    return rtn
