from canter import state
from canter.hal import Hal, Signal


class MockHal(Hal):
    def setup(self):
        pass

    def cleanup(self):
        pass

    def __init__(self, sig):
        super().__init__()
        self.sig = sig

    def signals(self) -> Signal:
        if self.sig is not None:
            return self.sig
        else:
            return Signal(False, False, False)

    def change_state(self, current_state, next_state):
        pass
