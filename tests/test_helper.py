from tests.helper import generate_current_state


def test_no_signals():
    t = generate_current_state(1, False)
    assert t.count_energized_motors() == 1
    assert t.motor1.energized is True

    t = generate_current_state(1, True)
    assert t.count_energized_motors() == 1

    t = generate_current_state(2, False)
    assert t.count_energized_motors() == 2
    assert t.motor1.energized is True
    assert t.motor2.energized is True

    t = generate_current_state(2, True)
    assert t.count_energized_motors() == 2

    t = generate_current_state(3, False)
    assert t.count_energized_motors() == 3
    assert t.motor1.energized is True
    assert t.motor2.energized is True
    assert t.motor3.energized is True

    t = generate_current_state(3, True)
    assert t.count_energized_motors() == 3