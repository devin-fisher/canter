import pytest

from canter import signals, state, hal
from tests.mock import MockHal
from tests.helper import *


def test_no_signals_from_off():
    test_state = signals.next_computed_state(
        generate_current_state(2, True),
        None,
        MockHal(None)
    )

    assert test_state is not None
    assert test_state.motor1.energized is False
    assert test_state.motor2.energized is False
    assert test_state.motor3.energized is False


def test_no_signals_from_on():
    test_state = signals.next_computed_state(
        generate_current_state(2, True),
        None,
        MockHal(None)
    )

    assert test_state is not None
    assert test_state.motor1.energized is False
    assert test_state.motor2.energized is False
    assert test_state.motor3.energized is False


def test_only_2_inch_pipe_from_off():
    test_state = signals.next_computed_state(
        state.off(),
        None,
        MockHal(hal.Signal(True, False, False))
    )

    assert test_state is not None
    assert test_state.count_energized_motors() == 1


def test_only_2_inch_pipe_from_on():
    current_state = generate_current_state(1, True)

    test_state = signals.next_computed_state(
        current_state,
        None,
        MockHal(hal.Signal(True, False, False))
    )

    assert test_state is not None
    assert test_state.count_energized_motors() == 1
    assert current_state.motor1.energized == test_state.motor1.energized
    assert current_state.motor2.energized == test_state.motor2.energized
    assert current_state.motor3.energized == test_state.motor3.energized


def test_only_2_inch_pipe_from_over_energized():
    current_state = generate_current_state(3, True)

    test_state = signals.next_computed_state(
        current_state,
        None,
        MockHal(hal.Signal(True, False, False))
    )

    assert test_state is not None
    assert test_state.count_energized_motors() == 1


def test_2_inch_pipe_with_turbo_from_off():
    test_state = signals.next_computed_state(
        state.off().switch_turbo(),
        None,
        MockHal(hal.Signal(True, False, False))
    )

    assert test_state is not None
    assert test_state.count_energized_motors() == 2


def test_only_4_inch_pipe_from_off():
    test_state = signal.next_computed_state(
        state.off(),
        None,
        MockHal(hal.Signal(False, True, False))
    )

    assert test_state is not None
    assert test_state.count_energized_motors() == 2


def test_only_4_inch_pipe_from_on():
    current_state = generate_current_state(2, True)

    test_state = signal.next_computed_state(
        current_state,
        None,
        MockHal(hal.Signal(False, True, False))
    )

    assert test_state is not None
    assert test_state.count_energized_motors() == 2
    assert current_state.motor1.energized == test_state.motor1.energized
    assert current_state.motor2.energized == test_state.motor2.energized
    assert current_state.motor3.energized == test_state.motor3.energized


def test_only_4_inch_pipe_from_over_energized():
    current_state = generate_current_state(3, True)

    test_state = signal.next_computed_state(
        current_state,
        None,
        MockHal(hal.Signal(False, True, False))
    )

    assert test_state is not None
    assert test_state.count_energized_motors() == 2


def test_4_inch_pipe_with_turbo_from_off():
    test_state = signal.next_computed_state(
        state.off().switch_turbo(),
        None,
        MockHal(hal.Signal(False, True, False))
    )

    assert test_state is not None
    assert test_state.count_energized_motors() == 3


def test_switching_turbo_from_off():
    test_state = signal.next_computed_state(
        state.off(),
        None,
        MockHal(hal.Signal(True, True, True))
    )

    assert test_state is not None
    assert test_state.count_energized_motors() == 0
    assert test_state.turbo is True


def test_switching_turbo_from_energized():
    current_state = generate_current_state(3, True, True)

    test_state = signal.next_computed_state(
        current_state,
        None,
        MockHal(hal.Signal(False, True, True))
    )

    assert test_state is not None
    assert test_state.count_energized_motors() == 3
    assert test_state.turbo is False


def test_turbo_without_motor_signal():
    current_state = generate_current_state(0, True, True)

    test_state = signal.next_computed_state(
        current_state,
        None,
        MockHal(hal.Signal(False, False, False))
    )

    assert test_state is not None
    assert test_state.turbo is True
    assert test_state.count_energized_motors() == 0
